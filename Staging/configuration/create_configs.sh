rm -f ./output/* &&
kubectl create configmap blockraiders-staging-game-config \
 --from-file=./configs -o yaml \
 --dry-run=client \
 -n blockraiders-staging-game \
>> output/blockraiders-staging-game-config.yaml
